﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Boolean ConGUI::visibleBt()
extern void ConGUI_visibleBt_mE3217882FF78FF247B1D0C92A5CC62E20236F808 (void);
// 0x00000002 System.Void ConGUI::Start()
extern void ConGUI_Start_m5B510BCAF2398B654457DF7480B802395469EE3E (void);
// 0x00000003 System.Void ConGUI::Update()
extern void ConGUI_Update_m8FC0F39729AFDEAED8619F8CA4FC91418F205048 (void);
// 0x00000004 System.Void ConGUI::OnGUI()
extern void ConGUI_OnGUI_mE07211D011CFAA984C3313DE27112BEA82167BBB (void);
// 0x00000005 System.Void ConGUI::effectOn()
extern void ConGUI_effectOn_m889DB42CAB87BCB4F65AF61D77772CC14C7EECA1 (void);
// 0x00000006 System.Void ConGUI::.ctor()
extern void ConGUI__ctor_m26544C2A9291C78B08DC68BE843ED0C2653C6C8C (void);
// 0x00000007 System.Void Lighting::Start()
extern void Lighting_Start_m06A2CEC31CF965351F7B5F320E6FE6E22164C8BA (void);
// 0x00000008 System.Void Lighting::Update()
extern void Lighting_Update_m3DE2B0C2A96152AE1F9B25D277C4CA3761208DFA (void);
// 0x00000009 System.Collections.IEnumerator Lighting::flash()
extern void Lighting_flash_m378CAD15BE0617306122445BA49A98DA153B203C (void);
// 0x0000000A System.Collections.IEnumerator Lighting::setRev()
extern void Lighting_setRev_m22C752FDFDF7D545E6C6DD6D94DC813E041710B9 (void);
// 0x0000000B System.Collections.IEnumerator Lighting::keepOn()
extern void Lighting_keepOn_m812ECE432F88D07A021AB2ED9D5B0C0AB7B37C9C (void);
// 0x0000000C System.Collections.IEnumerator Lighting::setFlashingOff()
extern void Lighting_setFlashingOff_m8BEA072167F411783B39CDFFC2814EBBF430BBB6 (void);
// 0x0000000D System.Void Lighting::.ctor()
extern void Lighting__ctor_m1969A80F89A008BE78AE2C8E457EC584645E48D5 (void);
// 0x0000000E System.Void Lighting/<flash>d__17::.ctor(System.Int32)
extern void U3CflashU3Ed__17__ctor_m51231BC85F83AA662AA03C7B8D6515F75084724D (void);
// 0x0000000F System.Void Lighting/<flash>d__17::System.IDisposable.Dispose()
extern void U3CflashU3Ed__17_System_IDisposable_Dispose_mE940F46AE5441A681C4C7ED621D9739506A021A6 (void);
// 0x00000010 System.Boolean Lighting/<flash>d__17::MoveNext()
extern void U3CflashU3Ed__17_MoveNext_mB6FDC7F456EED12BE4DE44C431ECAE620D1DED65 (void);
// 0x00000011 System.Object Lighting/<flash>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CflashU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8FB32E0E5541BA0309A45D48B8D368D53AE6F58B (void);
// 0x00000012 System.Void Lighting/<flash>d__17::System.Collections.IEnumerator.Reset()
extern void U3CflashU3Ed__17_System_Collections_IEnumerator_Reset_m1955DB6A785280DE98C051BF50CE2ECB28FD45A3 (void);
// 0x00000013 System.Object Lighting/<flash>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CflashU3Ed__17_System_Collections_IEnumerator_get_Current_mDDA110B0E41177C85655B45DF0607A367DE3F8D4 (void);
// 0x00000014 System.Void Lighting/<setRev>d__18::.ctor(System.Int32)
extern void U3CsetRevU3Ed__18__ctor_m1329C279F654CBECFE52EFBCCF2968F127E7368D (void);
// 0x00000015 System.Void Lighting/<setRev>d__18::System.IDisposable.Dispose()
extern void U3CsetRevU3Ed__18_System_IDisposable_Dispose_m94A5F396B46DE41AEA983772E6DA186CA5650653 (void);
// 0x00000016 System.Boolean Lighting/<setRev>d__18::MoveNext()
extern void U3CsetRevU3Ed__18_MoveNext_mBEF0B0DBA2F20FAB156DB31C0534A8022E6BA49C (void);
// 0x00000017 System.Object Lighting/<setRev>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CsetRevU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9FDDA8D15352CBEAB68B65367662D26A9C3DCB3E (void);
// 0x00000018 System.Void Lighting/<setRev>d__18::System.Collections.IEnumerator.Reset()
extern void U3CsetRevU3Ed__18_System_Collections_IEnumerator_Reset_mFD26FC3D0B3BED274C6CF3168EB6A2B77F6B5A2A (void);
// 0x00000019 System.Object Lighting/<setRev>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CsetRevU3Ed__18_System_Collections_IEnumerator_get_Current_mF8C89E4E3BF030E802E2429B684F9B50619E8F40 (void);
// 0x0000001A System.Void Lighting/<keepOn>d__19::.ctor(System.Int32)
extern void U3CkeepOnU3Ed__19__ctor_m6F8C9FC8CD8CEBEF452A5FFD9AF8406D0272553D (void);
// 0x0000001B System.Void Lighting/<keepOn>d__19::System.IDisposable.Dispose()
extern void U3CkeepOnU3Ed__19_System_IDisposable_Dispose_mD2D78E098B758CB84A5FE9017C72790331FA6116 (void);
// 0x0000001C System.Boolean Lighting/<keepOn>d__19::MoveNext()
extern void U3CkeepOnU3Ed__19_MoveNext_m013819EF7AE5D9ADECB1742BF52CD12B190CB2D9 (void);
// 0x0000001D System.Object Lighting/<keepOn>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CkeepOnU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m84A5C2E39695868F6F4C0C04282797C45E483846 (void);
// 0x0000001E System.Void Lighting/<keepOn>d__19::System.Collections.IEnumerator.Reset()
extern void U3CkeepOnU3Ed__19_System_Collections_IEnumerator_Reset_m9B8D6B0ABD5CDCF6B9E9ACAD879DBF3080CF2A37 (void);
// 0x0000001F System.Object Lighting/<keepOn>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CkeepOnU3Ed__19_System_Collections_IEnumerator_get_Current_m0CB456BC75B6EB2DDE1C0CFBD93B0F552BC0E68D (void);
// 0x00000020 System.Void Lighting/<setFlashingOff>d__20::.ctor(System.Int32)
extern void U3CsetFlashingOffU3Ed__20__ctor_m7FA48A624DE390576AF9BD8500AFE3CECDA63D5C (void);
// 0x00000021 System.Void Lighting/<setFlashingOff>d__20::System.IDisposable.Dispose()
extern void U3CsetFlashingOffU3Ed__20_System_IDisposable_Dispose_mDB32D5CA18C04B69DA00DE1A3F17A039D9AB7409 (void);
// 0x00000022 System.Boolean Lighting/<setFlashingOff>d__20::MoveNext()
extern void U3CsetFlashingOffU3Ed__20_MoveNext_mA3475396F7B754AD42FA87CAA593A35BE2E925D4 (void);
// 0x00000023 System.Object Lighting/<setFlashingOff>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CsetFlashingOffU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB1EE06573FADAFCF4E7C399F7252779761F1CB81 (void);
// 0x00000024 System.Void Lighting/<setFlashingOff>d__20::System.Collections.IEnumerator.Reset()
extern void U3CsetFlashingOffU3Ed__20_System_Collections_IEnumerator_Reset_mD6BA6F73DD21BBD80134A29D41C89226EE176329 (void);
// 0x00000025 System.Object Lighting/<setFlashingOff>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CsetFlashingOffU3Ed__20_System_Collections_IEnumerator_get_Current_mA5309D264C87AF7552771CC7BEBAAD30FB4439E4 (void);
// 0x00000026 System.Int32 BaseCharecter::get_Hp()
extern void BaseCharecter_get_Hp_m81F2BB3F1509BF7EDD1922ADB8E16F89AAE65B9C (void);
// 0x00000027 System.Void BaseCharecter::set_Hp(System.Int32)
extern void BaseCharecter_set_Hp_m70E246780EBB3EC3AE7E92F46EA7D7C2400362F0 (void);
// 0x00000028 System.Single BaseCharecter::get_Speed()
extern void BaseCharecter_get_Speed_mD446B1F8B341EE0FDFA90DDBE9AE9DCA03BB8EC2 (void);
// 0x00000029 System.Void BaseCharecter::set_Speed(System.Single)
extern void BaseCharecter_set_Speed_m8F2C700C4EF3D1E9F193A7B2FECBF69900577627 (void);
// 0x0000002A System.Single BaseCharecter::get_Jump()
extern void BaseCharecter_get_Jump_m8CEF559CD17628ADC30E24211610E096536BA7EF (void);
// 0x0000002B System.Void BaseCharecter::set_Jump(System.Single)
extern void BaseCharecter_set_Jump_m02E9FD4E6BF890385857E31130206B775848BD5A (void);
// 0x0000002C Bullet BaseCharecter::get_Bullet()
extern void BaseCharecter_get_Bullet_m6456AE43872957137EA547000F491867A330B1CE (void);
// 0x0000002D System.Void BaseCharecter::set_Bullet(Bullet)
extern void BaseCharecter_set_Bullet_mC7F01A7D7439DCAA8193AE3BBC6EE79861AA1E9C (void);
// 0x0000002E System.Void BaseCharecter::Init(System.Int32,System.Single,System.Single,Bullet)
extern void BaseCharecter_Init_m38AB19D5E93C1D7D225A130AF1EC5125AA4E9E85 (void);
// 0x0000002F System.Void BaseCharecter::Fire()
extern void BaseCharecter_Fire_m7B9367256C42752F23FCE7BCB265D4C02CB5B371 (void);
// 0x00000030 System.Void BaseCharecter::.ctor()
extern void BaseCharecter__ctor_mA649F881644A32A14D8BB610168F4F505634B665 (void);
// 0x00000031 System.Void Enemy::add_OnDie(System.Action)
extern void Enemy_add_OnDie_m6FC6A57088D8BBCEC99B0C8D7F15278E31227A89 (void);
// 0x00000032 System.Void Enemy::remove_OnDie(System.Action)
extern void Enemy_remove_OnDie_m5787B12B33E47457F20BE3E444961F01E64EFC6D (void);
// 0x00000033 System.Void Enemy::Fire()
extern void Enemy_Fire_m3F1F6B559F5C697843C9FDF06E1A7E62691FCF8E (void);
// 0x00000034 System.Void Enemy::Init(System.Int32,System.Single,System.Single)
extern void Enemy_Init_mC19675BD0CB7E8C730F8905DEF2486BF40316FE4 (void);
// 0x00000035 System.Void Enemy::TakeHit(System.Int32)
extern void Enemy_TakeHit_mB358E21FD5E28C7A601D06D97AE4359F7A5BA473 (void);
// 0x00000036 System.Void Enemy::Die()
extern void Enemy_Die_m9FD6BE305AA5A3AA47F4BA3E3D0DBFC39E4F7CFE (void);
// 0x00000037 System.Void Enemy::.ctor()
extern void Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734 (void);
// 0x00000038 System.Void EnemyCanon::add_OnDie(System.Action)
extern void EnemyCanon_add_OnDie_m2FF1056B7BC8AF1CF676EDA3173C184652AADA50 (void);
// 0x00000039 System.Void EnemyCanon::remove_OnDie(System.Action)
extern void EnemyCanon_remove_OnDie_mD3AA45E4D8CFC825B51D2DAA280828A44F761F59 (void);
// 0x0000003A System.Void EnemyCanon::Init(System.Int32,System.Single,System.Single)
extern void EnemyCanon_Init_m353FB7097041121CEACC05A35E2A90CF144ED667 (void);
// 0x0000003B System.Void EnemyCanon::TakeHit(System.Int32)
extern void EnemyCanon_TakeHit_mC9F4467CF0C964DEFC79C8DB60861AA572BCD890 (void);
// 0x0000003C System.Void EnemyCanon::Die()
extern void EnemyCanon_Die_m5E1CE861C6876C2F2961E0E79A77A7E713F93746 (void);
// 0x0000003D System.Void EnemyCanon::.ctor()
extern void EnemyCanon__ctor_mC90EDCCC34F086884C32617E04AACE14A68DC150 (void);
// 0x0000003E System.Void Player::add_OnDie(System.Action)
extern void Player_add_OnDie_mC5FC92921F700CA46C693F34A0FF5F7DD5238294 (void);
// 0x0000003F System.Void Player::remove_OnDie(System.Action)
extern void Player_remove_OnDie_mDFC7B81744158183CDB1D10B9240AD1EAE1D844F (void);
// 0x00000040 System.Void Player::Fire()
extern void Player_Fire_m85D1A8EF739313CCBDD710A5B5740D9AB4666790 (void);
// 0x00000041 System.Void Player::Init(System.Int32,System.Single,System.Single)
extern void Player_Init_m5D42BB6FD85D21008AB449DD8136B7CAE6CF95F1 (void);
// 0x00000042 System.Void Player::TakeHit(System.Int32)
extern void Player_TakeHit_mB08E890D180AEC9CBF0CD4FC343EA53369233B54 (void);
// 0x00000043 System.Void Player::TakeHeal(System.Int32)
extern void Player_TakeHeal_mF2BB8081DEA067BF9FDA53430266217C79F78BC7 (void);
// 0x00000044 System.Void Player::Die()
extern void Player_Die_m16A200929DBDF9FF88C8191A26327C2CCCC80C19 (void);
// 0x00000045 System.Void Player::.ctor()
extern void Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7 (void);
// 0x00000046 System.Void Bullet::Init(UnityEngine.Vector3)
extern void Bullet_Init_mE4A76CB0B1B83ED63EBD481F6A7F45F235D8EAFF (void);
// 0x00000047 System.Void Bullet::Awake()
extern void Bullet_Awake_m2D77C2A3CF11F66E86FF074B8C4397C0E3DE2004 (void);
// 0x00000048 System.Void Bullet::Move(UnityEngine.Vector3)
extern void Bullet_Move_m423477B277DE98F4108CA1C9DB89070ED2379A93 (void);
// 0x00000049 System.Void Bullet::OnTriggerEnter(UnityEngine.Collider)
extern void Bullet_OnTriggerEnter_m670A42C7BC93AF346496B6599303EF4C10FE690A (void);
// 0x0000004A System.Void Bullet::.ctor()
extern void Bullet__ctor_mC7D931FE508342F413FBA79525A4819D4114B3EC (void);
// 0x0000004B System.Void BulletCanon::OnTriggerEnter(UnityEngine.Collider)
extern void BulletCanon_OnTriggerEnter_m24DAF742F53E9CBEFED518735C7F17758A677D4C (void);
// 0x0000004C System.Void BulletCanon::.ctor()
extern void BulletCanon__ctor_m0F180EA56A00639DC0073C9EAEACBCF08070E622 (void);
// 0x0000004D System.Void BulletEnemy1::Init(UnityEngine.Vector3)
extern void BulletEnemy1_Init_mA8AC4401D8CE402BF77D4E66B4BFBAD6C3201911 (void);
// 0x0000004E System.Void BulletEnemy1::Awake()
extern void BulletEnemy1_Awake_mCC0C560E79541CAAB482EBB08C88EF26E01E3946 (void);
// 0x0000004F System.Void BulletEnemy1::Move(UnityEngine.Vector3)
extern void BulletEnemy1_Move_m599F206616ACDAD0436A87EB2043A03AD2B9822D (void);
// 0x00000050 System.Void BulletEnemy1::OnTriggerEnter(UnityEngine.Collider)
extern void BulletEnemy1_OnTriggerEnter_m0A040DF4F705BF17345F0EBC9AD1AD589639326C (void);
// 0x00000051 System.Void BulletEnemy1::.ctor()
extern void BulletEnemy1__ctor_m66CE002C774E1E80CC939DAF3ECC6221CBE2F1FC (void);
// 0x00000052 System.Void TimeToDestory::FixedUpdate()
extern void TimeToDestory_FixedUpdate_m3718EFBE33F3C2BE4912C11C8E4CE96D48DD44AE (void);
// 0x00000053 System.Void TimeToDestory::CountDownToDestory()
extern void TimeToDestory_CountDownToDestory_m960D9DE13C5782C4F1E98FDFDAD4E0C1EF43EE9E (void);
// 0x00000054 System.Void TimeToDestory::.ctor()
extern void TimeToDestory__ctor_m199CDAAA4194F246EF48FBA4B10B782650111017 (void);
// 0x00000055 System.Void Countinend::Start()
extern void Countinend_Start_m5155E47DAFDB5D90D45F0554F2E5268DD2D66C48 (void);
// 0x00000056 System.Void Countinend::OnNextText()
extern void Countinend_OnNextText_m8F5724CDCA71C0F885E8E546E7706EABC604DFA7 (void);
// 0x00000057 System.Void Countinend::OnApplicationQuit()
extern void Countinend_OnApplicationQuit_m78B9473059BA868796150AF30F514081132C56D8 (void);
// 0x00000058 System.Void Countinend::OnContinedGame()
extern void Countinend_OnContinedGame_m1DA466C1630987CBC81D5700AF29DB847A5DB89A (void);
// 0x00000059 System.Void Countinend::.ctor()
extern void Countinend__ctor_m01CC17A445AE65D1164CC666B1B48C4E59DFF546 (void);
// 0x0000005A System.Void MainMeun::Awake()
extern void MainMeun_Awake_m79E2441DA2DFF1EEC87367B2490F401CBDBFC5BD (void);
// 0x0000005B System.Void MainMeun::OnStartButtonOnClicked()
extern void MainMeun_OnStartButtonOnClicked_m95FFEFC1AAE30417A7CB02C0C6E36C6C5A1C69B1 (void);
// 0x0000005C System.Void MainMeun::OnApplicationQuit()
extern void MainMeun_OnApplicationQuit_mF54ED3FEA29BA3719FCCF04F149846637A331438 (void);
// 0x0000005D System.Void MainMeun::.ctor()
extern void MainMeun__ctor_m45056632ECFA6ED7F7588C2BDDFD46869F45B398 (void);
// 0x0000005E System.Void ResultMenu::Start()
extern void ResultMenu_Start_mBC7C1A2753FB0DC60DD33F0F6D60AD1CD857801B (void);
// 0x0000005F System.Void ResultMenu::Result()
extern void ResultMenu_Result_m0782BACD6332F9203A4092396D651E8456791667 (void);
// 0x00000060 System.Void ResultMenu::OnApplicationQuit()
extern void ResultMenu_OnApplicationQuit_m944496F0EEC9DE546BF501E6EE437696B267800B (void);
// 0x00000061 System.Void ResultMenu::.ctor()
extern void ResultMenu__ctor_m24E6B0FC89598164579C86E455244822CA5D5564 (void);
// 0x00000062 System.Void ReturnDialog::OnTriggerEnter(UnityEngine.Collider)
extern void ReturnDialog_OnTriggerEnter_m50A9514DCDC8A4647AFCD73E6BB05261744AC142 (void);
// 0x00000063 System.Void ReturnDialog::.ctor()
extern void ReturnDialog__ctor_mC6483B2884F9D5DE7ABD0306E5B71DC928F2154B (void);
// 0x00000064 System.Void EnemyController::Update()
extern void EnemyController_Update_mDB0B02F4008FD062F471267A67A06169E1BC1B3C (void);
// 0x00000065 System.Void EnemyController::ShootPlayer()
extern void EnemyController_ShootPlayer_m1ED355FA9A83BA43E6327B0F8994AF5ED8AF675F (void);
// 0x00000066 System.Void EnemyController::EnemyFire()
extern void EnemyController_EnemyFire_mACE28F8DCFA98F150AF6B433E671493C07CF24A1 (void);
// 0x00000067 System.Void EnemyController::OnDrawGizmosSelected()
extern void EnemyController_OnDrawGizmosSelected_m8009702CE33352E8EE2ACB15B8D2AB985C1E80AC (void);
// 0x00000068 System.Void EnemyController::.ctor()
extern void EnemyController__ctor_m547F49905D505F962CBC708846F8E8A3B0838F70 (void);
// 0x00000069 System.Void EnemyHealthBar::Start()
extern void EnemyHealthBar_Start_mAC5072644CD25461F54283A494648B93E10740AD (void);
// 0x0000006A System.Void EnemyHealthBar::.ctor()
extern void EnemyHealthBar__ctor_mA53148773B4E9CA545B3C7D4564ADAF769B50C8B (void);
// 0x0000006B System.Void HealBar::SetMaxHealth(System.Int32)
extern void HealBar_SetMaxHealth_m620B427A54A764D561CBC2E86E43952DEAB025F3 (void);
// 0x0000006C System.Void HealBar::SetHealth(System.Int32)
extern void HealBar_SetHealth_m3B0EA67B4D3E691A1C99DC87EE7D17AA17784A35 (void);
// 0x0000006D System.Void HealBar::.ctor()
extern void HealBar__ctor_mED7FEE8A6C0C7542EB3ACCB45AABFED2F4435131 (void);
// 0x0000006E System.Void Healing::Start()
extern void Healing_Start_m272DBF3E60B58AA4FA4BCA86906386E7D7A00831 (void);
// 0x0000006F System.Void Healing::FixedUpdate()
extern void Healing_FixedUpdate_mB38D412AFB95DB2596BEF277FAB02D7A42459354 (void);
// 0x00000070 System.Void Healing::Rotation()
extern void Healing_Rotation_mD1A7D5738E5E9B41420BA66E1C4BAD4727B0B33F (void);
// 0x00000071 System.Void Healing::OnTriggerEnter(UnityEngine.Collider)
extern void Healing_OnTriggerEnter_m58450FC37DDA40FF2F0B4CA356EFC74C33A5CAF9 (void);
// 0x00000072 System.Void Healing::.ctor()
extern void Healing__ctor_m26EB848ADB926535120EE9F28EBFAD54A917B2D5 (void);
// 0x00000073 System.Void IDamageable::TakeHit(System.Int32)
// 0x00000074 System.Void IDamageable::Die()
// 0x00000075 System.Void IHealingable::TakeHeal(System.Int32)
// 0x00000076 System.Void ScoreManager::SetScore(System.Int32)
extern void ScoreManager_SetScore_m86322F173E436E8E067EC97641031911830FD662 (void);
// 0x00000077 System.Void ScoreManager::.ctor()
extern void ScoreManager__ctor_m638A240D34643E8AB9D17553622C1C9354348354 (void);
// 0x00000078 System.Void ProjectTile::Update()
extern void ProjectTile_Update_m29ECED531369B6795BC79EDDB30DB1A672B4CB02 (void);
// 0x00000079 System.Void ProjectTile::LaunchProjectile()
extern void ProjectTile_LaunchProjectile_mEDDD616472EB4E515C351BB677BA7CCA58697878 (void);
// 0x0000007A UnityEngine.Vector3 ProjectTile::CalculateVelocity(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void ProjectTile_CalculateVelocity_mCDDDDE47630DCD3293093CEE1709243F62C3FEB0 (void);
// 0x0000007B System.Void ProjectTile::OnDrawGizmosSelected()
extern void ProjectTile_OnDrawGizmosSelected_m3DE3EDA6841FBB3CAF5BAEE98A36AE1EDDBA6FD5 (void);
// 0x0000007C System.Void ProjectTile::.ctor()
extern void ProjectTile__ctor_mEDF1C535C91D559315150D8D902F9C33A2EC610C (void);
// 0x0000007D System.Void PlayerController::Start()
extern void PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3 (void);
// 0x0000007E System.Void PlayerController::FixedUpdate()
extern void PlayerController_FixedUpdate_m54EE3ADAA7597303B1F69849B233D1A68D880B14 (void);
// 0x0000007F System.Void PlayerController::Update()
extern void PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794 (void);
// 0x00000080 System.Void PlayerController::Move()
extern void PlayerController_Move_mB16F20D8A54197A9CB7648A10810E68038A49E5D (void);
// 0x00000081 System.Void PlayerController::PlayerFire()
extern void PlayerController_PlayerFire_m77E461501F31D5070A85CBDEEEA752F892160DC1 (void);
// 0x00000082 System.Void PlayerController::PlayerDie()
extern void PlayerController_PlayerDie_m0783799F40BD63A907733979DC331E195DED7575 (void);
// 0x00000083 System.Boolean PlayerController::IsGrounded()
extern void PlayerController_IsGrounded_m3C163FC84A1D16A1BA93700227E0B283AA416C7D (void);
// 0x00000084 System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB (void);
// 0x00000085 System.Void PlayerHealthBar::Start()
extern void PlayerHealthBar_Start_mDC7E89DB99A6371857BF3548180A35E3D2581A99 (void);
// 0x00000086 System.Void PlayerHealthBar::.ctor()
extern void PlayerHealthBar__ctor_mEACC4D691595B746969FF17784DC787E96B9B59B (void);
// 0x00000087 System.Void PointDie::OnTriggerEnter(UnityEngine.Collider)
extern void PointDie_OnTriggerEnter_mB39A82586E1F895890FDF1307833C133FA94BDBA (void);
// 0x00000088 System.Void PointDie::.ctor()
extern void PointDie__ctor_m674993E6949D6B52DB587C4A1CF853AADE8BC2A8 (void);
// 0x00000089 System.Void SceneSwitch::OnTriggerEnter(UnityEngine.Collider)
extern void SceneSwitch_OnTriggerEnter_m19B1C23AA35986020361E319FAF624F7D3562E85 (void);
// 0x0000008A System.Void SceneSwitch::.ctor()
extern void SceneSwitch__ctor_m9ABB57EA17F1613A1B01E980F0342EF55E78F37F (void);
// 0x0000008B T Singleton`1::get_Instance()
// 0x0000008C System.Void Singleton`1::Awake()
// 0x0000008D System.Void Singleton`1::OnApplicationQuit()
// 0x0000008E System.Void Singleton`1::OnDestroy()
// 0x0000008F System.Void Singleton`1::.ctor()
// 0x00000090 System.Void Singleton`1::.cctor()
// 0x00000091 System.Void Manager.GameManager::Start()
extern void GameManager_Start_mC69A683D4A04B4F90D765739ADF7D6C38C69F0AF (void);
// 0x00000092 System.Void Manager.GameManager::OnEnemyDie()
extern void GameManager_OnEnemyDie_m974468BAF7AD5667C05023121B3EEB8A0EF650EE (void);
// 0x00000093 System.Void Manager.GameManager::OnPlayerDie()
extern void GameManager_OnPlayerDie_mB40A77CB223139CF5687DAAF9CF2F31F5EE067A5 (void);
// 0x00000094 System.Void Manager.GameManager::.ctor()
extern void GameManager__ctor_m4D37F4709FCE3942A010D8A64ECF2C2BA941D198 (void);
// 0x00000095 System.Void Manager.SoundManager::Start()
extern void SoundManager_Start_m658475EF80AC71899DDD38F2FF197C42BAA9386E (void);
// 0x00000096 System.Void Manager.SoundManager::Play(UnityEngine.AudioSource,Manager.SoundManager/Sound)
extern void SoundManager_Play_mCE2CB99074B67992C1CB0DC562603B48A3D4459B (void);
// 0x00000097 System.Void Manager.SoundManager::PlayBGM()
extern void SoundManager_PlayBGM_mDD9FEF1D0D605912278835E4213127B20F61D1B1 (void);
// 0x00000098 Manager.SoundManager/SoundClip Manager.SoundManager::GetSoundClip(Manager.SoundManager/Sound)
extern void SoundManager_GetSoundClip_m51C30CC4E5BFA93AFFC7942BF2DF677B28974509 (void);
// 0x00000099 System.Void Manager.SoundManager::.ctor()
extern void SoundManager__ctor_m82D5AF6FC16270F11B1EE8A880814DA38E044698 (void);
// 0x0000009A System.Void Manager.SpawnManager::Spawn()
extern void SpawnManager_Spawn_m0DEB676971B853455D6A1BD5819F0D5124E83282 (void);
// 0x0000009B System.Void Manager.SpawnManager::SpawnPlayer()
extern void SpawnManager_SpawnPlayer_m7E567F6A38F1F2879B09BB41449D1144642A575C (void);
// 0x0000009C System.Void Manager.SpawnManager::SpawnEnemy()
extern void SpawnManager_SpawnEnemy_m76AE471E0AA449379E3F79219BE9B799D7B9E7DB (void);
// 0x0000009D System.Void Manager.SpawnManager::SpawnCanonEnemy()
extern void SpawnManager_SpawnCanonEnemy_mA0DD44483D42AF83F5949144781137BE8F42339B (void);
// 0x0000009E System.Void Manager.SpawnManager::SpawnMapsLevelOne()
extern void SpawnManager_SpawnMapsLevelOne_m07E4751301D7755B3D5CD30C51DA8BF481A74FFD (void);
// 0x0000009F System.Void Manager.SpawnManager::SpawnMapsLevelTwo()
extern void SpawnManager_SpawnMapsLevelTwo_mAB416AA6E154AD1CCC85CE5D1AADE990CA4D9CD4 (void);
// 0x000000A0 System.Void Manager.SpawnManager::SpawnHeath()
extern void SpawnManager_SpawnHeath_mA6F93B55D28ED1C846A74795DDF4496448B2A39C (void);
// 0x000000A1 UnityEngine.Vector3 Manager.SpawnManager::GetSpawnMapLocationLevelOne()
extern void SpawnManager_GetSpawnMapLocationLevelOne_mA569F5998CDA3705E54DA659F44C70C14C5FDB85 (void);
// 0x000000A2 UnityEngine.Vector3 Manager.SpawnManager::GetSpawnMapLocationLevelTwo()
extern void SpawnManager_GetSpawnMapLocationLevelTwo_m8BEA7546AF5FA9CB4073C36C53909B75ADC4813B (void);
// 0x000000A3 UnityEngine.Vector3 Manager.SpawnManager::GetEnemyLocation()
extern void SpawnManager_GetEnemyLocation_m74A01696276A73CF61D61EE567B3EA07F68E96C3 (void);
// 0x000000A4 UnityEngine.Vector3 Manager.SpawnManager::GetHealthLocation()
extern void SpawnManager_GetHealthLocation_mE7B46BE9826FC2A682740ADA9582A1EBDA7AF061 (void);
// 0x000000A5 UnityEngine.Vector3 Manager.SpawnManager::GetCanonLocation()
extern void SpawnManager_GetCanonLocation_m9F359FFA41EFD14F941ED2D07C0A0FE0A7ED628F (void);
// 0x000000A6 System.Void Manager.SpawnManager::.ctor()
extern void SpawnManager__ctor_m57D3D88B19C7CE206E8FA6A9C0652DB785A19F22 (void);
static Il2CppMethodPointer s_methodPointers[166] = 
{
	ConGUI_visibleBt_mE3217882FF78FF247B1D0C92A5CC62E20236F808,
	ConGUI_Start_m5B510BCAF2398B654457DF7480B802395469EE3E,
	ConGUI_Update_m8FC0F39729AFDEAED8619F8CA4FC91418F205048,
	ConGUI_OnGUI_mE07211D011CFAA984C3313DE27112BEA82167BBB,
	ConGUI_effectOn_m889DB42CAB87BCB4F65AF61D77772CC14C7EECA1,
	ConGUI__ctor_m26544C2A9291C78B08DC68BE843ED0C2653C6C8C,
	Lighting_Start_m06A2CEC31CF965351F7B5F320E6FE6E22164C8BA,
	Lighting_Update_m3DE2B0C2A96152AE1F9B25D277C4CA3761208DFA,
	Lighting_flash_m378CAD15BE0617306122445BA49A98DA153B203C,
	Lighting_setRev_m22C752FDFDF7D545E6C6DD6D94DC813E041710B9,
	Lighting_keepOn_m812ECE432F88D07A021AB2ED9D5B0C0AB7B37C9C,
	Lighting_setFlashingOff_m8BEA072167F411783B39CDFFC2814EBBF430BBB6,
	Lighting__ctor_m1969A80F89A008BE78AE2C8E457EC584645E48D5,
	U3CflashU3Ed__17__ctor_m51231BC85F83AA662AA03C7B8D6515F75084724D,
	U3CflashU3Ed__17_System_IDisposable_Dispose_mE940F46AE5441A681C4C7ED621D9739506A021A6,
	U3CflashU3Ed__17_MoveNext_mB6FDC7F456EED12BE4DE44C431ECAE620D1DED65,
	U3CflashU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8FB32E0E5541BA0309A45D48B8D368D53AE6F58B,
	U3CflashU3Ed__17_System_Collections_IEnumerator_Reset_m1955DB6A785280DE98C051BF50CE2ECB28FD45A3,
	U3CflashU3Ed__17_System_Collections_IEnumerator_get_Current_mDDA110B0E41177C85655B45DF0607A367DE3F8D4,
	U3CsetRevU3Ed__18__ctor_m1329C279F654CBECFE52EFBCCF2968F127E7368D,
	U3CsetRevU3Ed__18_System_IDisposable_Dispose_m94A5F396B46DE41AEA983772E6DA186CA5650653,
	U3CsetRevU3Ed__18_MoveNext_mBEF0B0DBA2F20FAB156DB31C0534A8022E6BA49C,
	U3CsetRevU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9FDDA8D15352CBEAB68B65367662D26A9C3DCB3E,
	U3CsetRevU3Ed__18_System_Collections_IEnumerator_Reset_mFD26FC3D0B3BED274C6CF3168EB6A2B77F6B5A2A,
	U3CsetRevU3Ed__18_System_Collections_IEnumerator_get_Current_mF8C89E4E3BF030E802E2429B684F9B50619E8F40,
	U3CkeepOnU3Ed__19__ctor_m6F8C9FC8CD8CEBEF452A5FFD9AF8406D0272553D,
	U3CkeepOnU3Ed__19_System_IDisposable_Dispose_mD2D78E098B758CB84A5FE9017C72790331FA6116,
	U3CkeepOnU3Ed__19_MoveNext_m013819EF7AE5D9ADECB1742BF52CD12B190CB2D9,
	U3CkeepOnU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m84A5C2E39695868F6F4C0C04282797C45E483846,
	U3CkeepOnU3Ed__19_System_Collections_IEnumerator_Reset_m9B8D6B0ABD5CDCF6B9E9ACAD879DBF3080CF2A37,
	U3CkeepOnU3Ed__19_System_Collections_IEnumerator_get_Current_m0CB456BC75B6EB2DDE1C0CFBD93B0F552BC0E68D,
	U3CsetFlashingOffU3Ed__20__ctor_m7FA48A624DE390576AF9BD8500AFE3CECDA63D5C,
	U3CsetFlashingOffU3Ed__20_System_IDisposable_Dispose_mDB32D5CA18C04B69DA00DE1A3F17A039D9AB7409,
	U3CsetFlashingOffU3Ed__20_MoveNext_mA3475396F7B754AD42FA87CAA593A35BE2E925D4,
	U3CsetFlashingOffU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB1EE06573FADAFCF4E7C399F7252779761F1CB81,
	U3CsetFlashingOffU3Ed__20_System_Collections_IEnumerator_Reset_mD6BA6F73DD21BBD80134A29D41C89226EE176329,
	U3CsetFlashingOffU3Ed__20_System_Collections_IEnumerator_get_Current_mA5309D264C87AF7552771CC7BEBAAD30FB4439E4,
	BaseCharecter_get_Hp_m81F2BB3F1509BF7EDD1922ADB8E16F89AAE65B9C,
	BaseCharecter_set_Hp_m70E246780EBB3EC3AE7E92F46EA7D7C2400362F0,
	BaseCharecter_get_Speed_mD446B1F8B341EE0FDFA90DDBE9AE9DCA03BB8EC2,
	BaseCharecter_set_Speed_m8F2C700C4EF3D1E9F193A7B2FECBF69900577627,
	BaseCharecter_get_Jump_m8CEF559CD17628ADC30E24211610E096536BA7EF,
	BaseCharecter_set_Jump_m02E9FD4E6BF890385857E31130206B775848BD5A,
	BaseCharecter_get_Bullet_m6456AE43872957137EA547000F491867A330B1CE,
	BaseCharecter_set_Bullet_mC7F01A7D7439DCAA8193AE3BBC6EE79861AA1E9C,
	BaseCharecter_Init_m38AB19D5E93C1D7D225A130AF1EC5125AA4E9E85,
	BaseCharecter_Fire_m7B9367256C42752F23FCE7BCB265D4C02CB5B371,
	BaseCharecter__ctor_mA649F881644A32A14D8BB610168F4F505634B665,
	Enemy_add_OnDie_m6FC6A57088D8BBCEC99B0C8D7F15278E31227A89,
	Enemy_remove_OnDie_m5787B12B33E47457F20BE3E444961F01E64EFC6D,
	Enemy_Fire_m3F1F6B559F5C697843C9FDF06E1A7E62691FCF8E,
	Enemy_Init_mC19675BD0CB7E8C730F8905DEF2486BF40316FE4,
	Enemy_TakeHit_mB358E21FD5E28C7A601D06D97AE4359F7A5BA473,
	Enemy_Die_m9FD6BE305AA5A3AA47F4BA3E3D0DBFC39E4F7CFE,
	Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734,
	EnemyCanon_add_OnDie_m2FF1056B7BC8AF1CF676EDA3173C184652AADA50,
	EnemyCanon_remove_OnDie_mD3AA45E4D8CFC825B51D2DAA280828A44F761F59,
	EnemyCanon_Init_m353FB7097041121CEACC05A35E2A90CF144ED667,
	EnemyCanon_TakeHit_mC9F4467CF0C964DEFC79C8DB60861AA572BCD890,
	EnemyCanon_Die_m5E1CE861C6876C2F2961E0E79A77A7E713F93746,
	EnemyCanon__ctor_mC90EDCCC34F086884C32617E04AACE14A68DC150,
	Player_add_OnDie_mC5FC92921F700CA46C693F34A0FF5F7DD5238294,
	Player_remove_OnDie_mDFC7B81744158183CDB1D10B9240AD1EAE1D844F,
	Player_Fire_m85D1A8EF739313CCBDD710A5B5740D9AB4666790,
	Player_Init_m5D42BB6FD85D21008AB449DD8136B7CAE6CF95F1,
	Player_TakeHit_mB08E890D180AEC9CBF0CD4FC343EA53369233B54,
	Player_TakeHeal_mF2BB8081DEA067BF9FDA53430266217C79F78BC7,
	Player_Die_m16A200929DBDF9FF88C8191A26327C2CCCC80C19,
	Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7,
	Bullet_Init_mE4A76CB0B1B83ED63EBD481F6A7F45F235D8EAFF,
	Bullet_Awake_m2D77C2A3CF11F66E86FF074B8C4397C0E3DE2004,
	Bullet_Move_m423477B277DE98F4108CA1C9DB89070ED2379A93,
	Bullet_OnTriggerEnter_m670A42C7BC93AF346496B6599303EF4C10FE690A,
	Bullet__ctor_mC7D931FE508342F413FBA79525A4819D4114B3EC,
	BulletCanon_OnTriggerEnter_m24DAF742F53E9CBEFED518735C7F17758A677D4C,
	BulletCanon__ctor_m0F180EA56A00639DC0073C9EAEACBCF08070E622,
	BulletEnemy1_Init_mA8AC4401D8CE402BF77D4E66B4BFBAD6C3201911,
	BulletEnemy1_Awake_mCC0C560E79541CAAB482EBB08C88EF26E01E3946,
	BulletEnemy1_Move_m599F206616ACDAD0436A87EB2043A03AD2B9822D,
	BulletEnemy1_OnTriggerEnter_m0A040DF4F705BF17345F0EBC9AD1AD589639326C,
	BulletEnemy1__ctor_m66CE002C774E1E80CC939DAF3ECC6221CBE2F1FC,
	TimeToDestory_FixedUpdate_m3718EFBE33F3C2BE4912C11C8E4CE96D48DD44AE,
	TimeToDestory_CountDownToDestory_m960D9DE13C5782C4F1E98FDFDAD4E0C1EF43EE9E,
	TimeToDestory__ctor_m199CDAAA4194F246EF48FBA4B10B782650111017,
	Countinend_Start_m5155E47DAFDB5D90D45F0554F2E5268DD2D66C48,
	Countinend_OnNextText_m8F5724CDCA71C0F885E8E546E7706EABC604DFA7,
	Countinend_OnApplicationQuit_m78B9473059BA868796150AF30F514081132C56D8,
	Countinend_OnContinedGame_m1DA466C1630987CBC81D5700AF29DB847A5DB89A,
	Countinend__ctor_m01CC17A445AE65D1164CC666B1B48C4E59DFF546,
	MainMeun_Awake_m79E2441DA2DFF1EEC87367B2490F401CBDBFC5BD,
	MainMeun_OnStartButtonOnClicked_m95FFEFC1AAE30417A7CB02C0C6E36C6C5A1C69B1,
	MainMeun_OnApplicationQuit_mF54ED3FEA29BA3719FCCF04F149846637A331438,
	MainMeun__ctor_m45056632ECFA6ED7F7588C2BDDFD46869F45B398,
	ResultMenu_Start_mBC7C1A2753FB0DC60DD33F0F6D60AD1CD857801B,
	ResultMenu_Result_m0782BACD6332F9203A4092396D651E8456791667,
	ResultMenu_OnApplicationQuit_m944496F0EEC9DE546BF501E6EE437696B267800B,
	ResultMenu__ctor_m24E6B0FC89598164579C86E455244822CA5D5564,
	ReturnDialog_OnTriggerEnter_m50A9514DCDC8A4647AFCD73E6BB05261744AC142,
	ReturnDialog__ctor_mC6483B2884F9D5DE7ABD0306E5B71DC928F2154B,
	EnemyController_Update_mDB0B02F4008FD062F471267A67A06169E1BC1B3C,
	EnemyController_ShootPlayer_m1ED355FA9A83BA43E6327B0F8994AF5ED8AF675F,
	EnemyController_EnemyFire_mACE28F8DCFA98F150AF6B433E671493C07CF24A1,
	EnemyController_OnDrawGizmosSelected_m8009702CE33352E8EE2ACB15B8D2AB985C1E80AC,
	EnemyController__ctor_m547F49905D505F962CBC708846F8E8A3B0838F70,
	EnemyHealthBar_Start_mAC5072644CD25461F54283A494648B93E10740AD,
	EnemyHealthBar__ctor_mA53148773B4E9CA545B3C7D4564ADAF769B50C8B,
	HealBar_SetMaxHealth_m620B427A54A764D561CBC2E86E43952DEAB025F3,
	HealBar_SetHealth_m3B0EA67B4D3E691A1C99DC87EE7D17AA17784A35,
	HealBar__ctor_mED7FEE8A6C0C7542EB3ACCB45AABFED2F4435131,
	Healing_Start_m272DBF3E60B58AA4FA4BCA86906386E7D7A00831,
	Healing_FixedUpdate_mB38D412AFB95DB2596BEF277FAB02D7A42459354,
	Healing_Rotation_mD1A7D5738E5E9B41420BA66E1C4BAD4727B0B33F,
	Healing_OnTriggerEnter_m58450FC37DDA40FF2F0B4CA356EFC74C33A5CAF9,
	Healing__ctor_m26EB848ADB926535120EE9F28EBFAD54A917B2D5,
	NULL,
	NULL,
	NULL,
	ScoreManager_SetScore_m86322F173E436E8E067EC97641031911830FD662,
	ScoreManager__ctor_m638A240D34643E8AB9D17553622C1C9354348354,
	ProjectTile_Update_m29ECED531369B6795BC79EDDB30DB1A672B4CB02,
	ProjectTile_LaunchProjectile_mEDDD616472EB4E515C351BB677BA7CCA58697878,
	ProjectTile_CalculateVelocity_mCDDDDE47630DCD3293093CEE1709243F62C3FEB0,
	ProjectTile_OnDrawGizmosSelected_m3DE3EDA6841FBB3CAF5BAEE98A36AE1EDDBA6FD5,
	ProjectTile__ctor_mEDF1C535C91D559315150D8D902F9C33A2EC610C,
	PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3,
	PlayerController_FixedUpdate_m54EE3ADAA7597303B1F69849B233D1A68D880B14,
	PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794,
	PlayerController_Move_mB16F20D8A54197A9CB7648A10810E68038A49E5D,
	PlayerController_PlayerFire_m77E461501F31D5070A85CBDEEEA752F892160DC1,
	PlayerController_PlayerDie_m0783799F40BD63A907733979DC331E195DED7575,
	PlayerController_IsGrounded_m3C163FC84A1D16A1BA93700227E0B283AA416C7D,
	PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB,
	PlayerHealthBar_Start_mDC7E89DB99A6371857BF3548180A35E3D2581A99,
	PlayerHealthBar__ctor_mEACC4D691595B746969FF17784DC787E96B9B59B,
	PointDie_OnTriggerEnter_mB39A82586E1F895890FDF1307833C133FA94BDBA,
	PointDie__ctor_m674993E6949D6B52DB587C4A1CF853AADE8BC2A8,
	SceneSwitch_OnTriggerEnter_m19B1C23AA35986020361E319FAF624F7D3562E85,
	SceneSwitch__ctor_m9ABB57EA17F1613A1B01E980F0342EF55E78F37F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	GameManager_Start_mC69A683D4A04B4F90D765739ADF7D6C38C69F0AF,
	GameManager_OnEnemyDie_m974468BAF7AD5667C05023121B3EEB8A0EF650EE,
	GameManager_OnPlayerDie_mB40A77CB223139CF5687DAAF9CF2F31F5EE067A5,
	GameManager__ctor_m4D37F4709FCE3942A010D8A64ECF2C2BA941D198,
	SoundManager_Start_m658475EF80AC71899DDD38F2FF197C42BAA9386E,
	SoundManager_Play_mCE2CB99074B67992C1CB0DC562603B48A3D4459B,
	SoundManager_PlayBGM_mDD9FEF1D0D605912278835E4213127B20F61D1B1,
	SoundManager_GetSoundClip_m51C30CC4E5BFA93AFFC7942BF2DF677B28974509,
	SoundManager__ctor_m82D5AF6FC16270F11B1EE8A880814DA38E044698,
	SpawnManager_Spawn_m0DEB676971B853455D6A1BD5819F0D5124E83282,
	SpawnManager_SpawnPlayer_m7E567F6A38F1F2879B09BB41449D1144642A575C,
	SpawnManager_SpawnEnemy_m76AE471E0AA449379E3F79219BE9B799D7B9E7DB,
	SpawnManager_SpawnCanonEnemy_mA0DD44483D42AF83F5949144781137BE8F42339B,
	SpawnManager_SpawnMapsLevelOne_m07E4751301D7755B3D5CD30C51DA8BF481A74FFD,
	SpawnManager_SpawnMapsLevelTwo_mAB416AA6E154AD1CCC85CE5D1AADE990CA4D9CD4,
	SpawnManager_SpawnHeath_mA6F93B55D28ED1C846A74795DDF4496448B2A39C,
	SpawnManager_GetSpawnMapLocationLevelOne_mA569F5998CDA3705E54DA659F44C70C14C5FDB85,
	SpawnManager_GetSpawnMapLocationLevelTwo_m8BEA7546AF5FA9CB4073C36C53909B75ADC4813B,
	SpawnManager_GetEnemyLocation_m74A01696276A73CF61D61EE567B3EA07F68E96C3,
	SpawnManager_GetHealthLocation_mE7B46BE9826FC2A682740ADA9582A1EBDA7AF061,
	SpawnManager_GetCanonLocation_m9F359FFA41EFD14F941ED2D07C0A0FE0A7ED628F,
	SpawnManager__ctor_m57D3D88B19C7CE206E8FA6A9C0652DB785A19F22,
};
static const int32_t s_InvokerIndices[166] = 
{
	1834,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1812,
	1812,
	1812,
	1812,
	1858,
	1530,
	1858,
	1834,
	1812,
	1858,
	1812,
	1530,
	1858,
	1834,
	1812,
	1858,
	1812,
	1530,
	1858,
	1834,
	1812,
	1858,
	1812,
	1530,
	1858,
	1834,
	1812,
	1858,
	1812,
	1798,
	1530,
	1836,
	1562,
	1836,
	1562,
	1812,
	1543,
	366,
	1858,
	1858,
	1543,
	1543,
	1858,
	573,
	1530,
	1858,
	1858,
	1543,
	1543,
	573,
	1530,
	1858,
	1858,
	1543,
	1543,
	1858,
	573,
	1530,
	1530,
	1858,
	1858,
	1582,
	1858,
	1582,
	1543,
	1858,
	1543,
	1858,
	1582,
	1858,
	1582,
	1543,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1543,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1530,
	1530,
	1858,
	1858,
	1858,
	1858,
	1543,
	1858,
	1530,
	1858,
	1530,
	1530,
	1858,
	1858,
	1858,
	548,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1834,
	1858,
	1858,
	1858,
	1543,
	1858,
	1543,
	1858,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1858,
	1858,
	1858,
	1858,
	1858,
	962,
	1858,
	1646,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1858,
	1855,
	1855,
	1855,
	1855,
	1855,
	1858,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x02000020, { 0, 5 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[5] = 
{
	{ (Il2CppRGCTXDataType)2, 1669 },
	{ (Il2CppRGCTXDataType)2, 296 },
	{ (Il2CppRGCTXDataType)1, 296 },
	{ (Il2CppRGCTXDataType)3, 10560 },
	{ (Il2CppRGCTXDataType)3, 8030 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	166,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	5,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
