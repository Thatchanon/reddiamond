﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Manager;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Rigidbody rb;
    [SerializeField] private Player player;
    [SerializeField] private float coolDown;
    [SerializeField] private float timeToFire;
    private float distToGround;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        distToGround = GetComponent<Collider>().bounds.extents.y;
    }
    private void FixedUpdate()
    {
        Move();
    }
    private void Update()
    {
        PlayerFire();
    }
    private void Move()
    {
        bool inputA = Input.GetKey(KeyCode.A);
        bool inputD = Input.GetKey(KeyCode.D);
        bool inputW = Input.GetKey(KeyCode.W);
        if (inputA)
        {
            transform.Translate(0, 0, -GameManager.Instance.playerSpeed * Time.deltaTime);
        }
        if (inputD)
        {
            transform.Translate(0, 0, GameManager.Instance.playerSpeed * Time.deltaTime);
        }

        if (inputW && IsGrounded()) 
        {
            Vector3 atas = new Vector3(0, GameManager.Instance.playerJump, 0);
            rb.AddForce(atas * GameManager.Instance.playerSpeed);
        }
    }
    private void PlayerFire()
    {
        coolDown += Time.deltaTime;
        if (Input.GetMouseButtonDown(0) && IsGrounded()) 
        {
            if(coolDown >= timeToFire)
            {
                player.Fire();
                coolDown = 0;
            }
        }
    }
    private void PlayerDie()
    {
        var playerDie = player.transform.position.y;
        {

        }
    }
    private bool IsGrounded()
    {
        return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f);
    }
}
