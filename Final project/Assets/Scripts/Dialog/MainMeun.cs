﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;
using Manager;

public class MainMeun : MonoBehaviour
{   
    [SerializeField] private Button startButton;
    [SerializeField] private Button quitButton;
    public RectTransform dialogMainMenu;
    private void Awake()
    {
        startButton.onClick.AddListener(OnStartButtonOnClicked);
        quitButton.onClick.AddListener(OnApplicationQuit);
    }
    public void OnStartButtonOnClicked()
    {
        dialogMainMenu.gameObject.SetActive(false);
        SpawnManager.Instance.Spawn();
    }
    private void OnApplicationQuit()
    {
        Application.Quit();
    }
}

