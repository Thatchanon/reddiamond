﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ResultMenu : MonoBehaviour
{
    [SerializeField] private RectTransform dialogResult;
    [SerializeField] private Button quitButton;
    public MainMeun mainMeun;
    private void Start()
    {
        quitButton.onClick.AddListener(OnApplicationQuit);
    }
    public void Result()
    {
        dialogResult.gameObject.SetActive(true);
    }
    private void OnApplicationQuit()
    {
        Application.Quit();
    }
}
