﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnDialog : MonoBehaviour
{
    [SerializeField] private ResultMenu resultMenu;
    private void OnTriggerEnter(Collider other)
    {
        resultMenu.Result();
    }
}
