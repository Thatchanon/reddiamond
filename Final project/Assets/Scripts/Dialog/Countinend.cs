﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Manager;
using TMPro;
public class Countinend : MonoBehaviour
{
    [SerializeField] private Button nextButton;
    [SerializeField] private Button quitButton;
    [SerializeField] private Button continedButton;
    public RectTransform dialogContined;
    [SerializeField] private TextMeshProUGUI textBody;
    private void Start()
    {
        nextButton.onClick.AddListener(OnNextText);
        quitButton.onClick.AddListener(OnApplicationQuit);
        continedButton.onClick.AddListener(OnContinedGame);
    }
    private void OnNextText()
    {
        textBody.text = $"the enemy has change to canon if canon shoot you and hit you will die.";
        nextButton.gameObject.SetActive(false);
        continedButton.gameObject.SetActive(true);
    }
    private void OnApplicationQuit()
    {
        Application.Quit();
    }
    private void OnContinedGame()
    {
        dialogContined.gameObject.SetActive(false);
        SpawnManager.Instance.SpawnCanonEnemy();
        SpawnManager.Instance.SpawnMapsLevelTwo();
    }
}
