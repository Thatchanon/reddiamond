﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private float coolDown;
    [SerializeField] private float Fire;
    [SerializeField] private float lookRadius;
    [SerializeField] private Enemy enemy;
    bool fristTime = true;
    private void Update()
    {
        ShootPlayer();
    }
    private void ShootPlayer()
    {
        var target = FindObjectOfType<Player>();
        if (target == null) 
        {
            return;
        }
        var distance = Vector3.Distance(transform.position, target.transform.position);
        if(distance < lookRadius)
        {
            EnemyFire();
        }
    }
    private void EnemyFire()
    {   
        
        if (fristTime)
        {   
            enemy.Fire();
            fristTime = false;
        }
        coolDown += Time.deltaTime;
        if(coolDown >= Fire)
        {
            enemy.Fire();
            coolDown = 0;
        }
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius); 
    }
}
