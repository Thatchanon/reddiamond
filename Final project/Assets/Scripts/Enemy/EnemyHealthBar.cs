﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Manager;
public class EnemyHealthBar : MonoBehaviour
{
    public HealBar healBar;
    private void Start()
    {
        healBar.SetMaxHealth(GameManager.Instance.enemyHp);
    }
}
