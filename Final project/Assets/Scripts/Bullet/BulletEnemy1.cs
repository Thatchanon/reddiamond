using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEnemy1 : MonoBehaviour
{
    [SerializeField] private int damage;
    [SerializeField] private float speed;
    [SerializeField] private Rigidbody rb;
    public void Init(Vector3 direction)
    {
        Move(direction);
    }
    private void Awake()
    {
        Debug.Assert(rb != null, "rigidbody canot be null");
    }
    private void Move(Vector3 direction)
    {
        rb.velocity = direction * speed;
    }
    public void OnTriggerEnter(Collider other)
    {
        var target = other.gameObject.GetComponent<IDamageable>();
        target?.TakeHit(damage);
        Destroy(gameObject);
    }
}
