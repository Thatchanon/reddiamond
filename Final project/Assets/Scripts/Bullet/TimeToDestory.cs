﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeToDestory : MonoBehaviour
{
    [SerializeField] private float timeCount;
    [SerializeField] private float timePerPoint;
    private void FixedUpdate()
    {
        CountDownToDestory();
    }
    private void CountDownToDestory()
    {
        timeCount += Time.deltaTime;
        if (timeCount > timePerPoint)
        {
            Destroy(gameObject);
        }
    }
}
