﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCanon : MonoBehaviour
{
    [SerializeField] private int damage;
    [SerializeField] private Rigidbody rb;
    
    public void OnTriggerEnter(Collider other)
    {
        var target = other.gameObject.GetComponent<IDamageable>();
        target?.TakeHit(damage);
        Destroy(gameObject);
    }
}
