﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseCharecter : MonoBehaviour
{
    [SerializeField] protected Bullet defaultBullet;
    [SerializeField] protected Transform spawnBullet;
    public int Hp { get; protected set; }
    public float Speed { get; protected set; }
    public float Jump { get; protected set; }
    public Bullet Bullet { get; private set; }
    protected virtual void Init(int hp, float speed, float jump, Bullet bullet)
    {
        Hp = hp;
        Speed = speed;
        Jump = jump;
        Bullet = bullet;
    }
    public virtual void Fire()
    {
        //Fire;
    }
}
