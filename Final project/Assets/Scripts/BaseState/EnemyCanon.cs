﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Manager;
public class EnemyCanon : BaseCharecter, IDamageable
{
    public event Action OnDie;
    [SerializeField] private EnemyHealthBar enemyHealthBar;
    public void Init(int hp, float speed, float jump)
    {
        base.Init(hp, speed, jump, defaultBullet);
    }
    public void TakeHit(int damage)
    {
        Hp -= damage;
        enemyHealthBar.healBar.SetHealth(Hp);
        SoundManager.Instance.Play(SoundManager.Instance.audioSourceAction, SoundManager.Sound.Enemyhit);
        if (Hp > 0)
        {
            return;
        }
        Die();
    }
    public void Die()
    {
        Debug.Assert(Hp <= 0, "Hp is more than zero");
        SoundManager.Instance.Play(SoundManager.Instance.audioSourceAction, SoundManager.Sound.Enemydeath);
        gameObject.SetActive(false);
        Destroy(gameObject);
        OnDie?.Invoke();
    }
}
