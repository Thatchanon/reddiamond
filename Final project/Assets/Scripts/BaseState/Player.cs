﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using Manager;
public class Player : BaseCharecter, IDamageable , IHealingable
{
    public event Action OnDie;
    [SerializeField] private PlayerHealthBar playerHealthBar;
    public override void Fire()
    {
        base.Fire();
        var bullet = Instantiate(defaultBullet, spawnBullet.position, Quaternion.identity);
        SoundManager.Instance.Play(SoundManager.Instance.audioSourceAction, SoundManager.Sound.Playerfire);
        bullet.Init(Vector3.forward);
    }
    public void Init(int hp, float speed, float jump)
    {
        base.Init(hp, speed, jump, defaultBullet);
    }
    public void TakeHit(int damage)
    {
        Hp -= damage;
        SoundManager.Instance.Play(SoundManager.Instance.audioSourceAction, SoundManager.Sound.Playerhit);
        playerHealthBar.healBar.SetHealth(Hp);
        if (Hp > 0)
        {
            return;
        }
        Die();
    }
    public void TakeHeal(int heal)
    {
        Hp += heal;
        playerHealthBar.healBar.SetHealth(Hp);
        if (Hp >= GameManager.Instance.playerHp)
        {
            Hp = GameManager.Instance.playerHp;
            return;
        }
    }
    public void Die()
    {
        SoundManager.Instance.Play(SoundManager.Instance.audioSourceAction, SoundManager.Sound.Playerdeath);
        Debug.Log("check die");
        Debug.Assert(Hp <= 0, "Hp is more than zero");
        gameObject.SetActive(false);
        Destroy(gameObject);
        OnDie?.Invoke();
    }

    
}
