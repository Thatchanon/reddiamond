﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManager : Singleton<ScoreManager>
{
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI finalScoreText;
    private int playerScore;
    public void SetScore(int score)
    {
        var total = playerScore += score;
        scoreText.text = $"Score : {total}";
        finalScoreText.text = $"Your Socre : {total}";
    }
}
