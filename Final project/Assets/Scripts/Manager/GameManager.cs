﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace Manager
{
    public class GameManager : Singleton<GameManager>
    {
        [SerializeField] private SpawnManager spawnManager;
        [SerializeField] private ResultMenu resultMenu;
        [Header("Player")]
        public int playerHp;
        public float playerSpeed;
        public float playerJump;

        [Header("Enemy")]
        public int enemyHp;
        public float enemySpeed;
        public float enemyJump;

        [Header("CanonEmemy")]
        public int canonEmemyHp;
        public float canonEmemySpeed;
        public float canonEmemyJump;

        [Space]
        public Player player;
        public Enemy enemy;
        public EnemyCanon enemyCanon;
        private void Start()
        {
            Debug.Assert(player != null, "player cannot be null");
            Debug.Assert(enemy != null, "enemy cannot be null");
            Debug.Assert(enemyCanon != null, "enemyCanon cannot bt null");
            SoundManager.Instance.PlayBGM();
        }
        public void OnEnemyDie()
        {
            ScoreManager.Instance.SetScore(1);
        }
        public void OnPlayerDie()
        {
            resultMenu.Result();
        }
    }
}
