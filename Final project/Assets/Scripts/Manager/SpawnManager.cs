﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager
{
    public class SpawnManager : Singleton<SpawnManager>
    {
        [SerializeField] private int numberSpawnHealth;
        [SerializeField] private int numberSpawnEnemys;
        [SerializeField] private int numberSpawnMaps;
        [SerializeField] private int numberSpawnCanonEnemys;
        [SerializeField] private List<Transform> spawnEnemyPoints;
        [SerializeField] private List<Transform> spawnMapLevelOnePoints;
        [SerializeField] private List<Transform> spawnMapLevelTwoPoints;
        [SerializeField] private List<Transform> spawnHealthPoionts;
        [SerializeField] private List<Transform> spawnCanonPoints;
        [SerializeField] private GameObject[] map;
        [SerializeField] private GameObject heath;

        public void Spawn()
        {
            SpawnPlayer();
            SpawnEnemy();
            SpawnMapsLevelOne();
            SpawnHeath();
        }
        public void SpawnPlayer()
        {
            var spawnPlayer = Instantiate(GameManager.Instance.player);
            spawnPlayer.Init(GameManager.Instance.playerHp, GameManager.Instance.playerSpeed, GameManager.Instance.playerJump);
            spawnPlayer.OnDie += GameManager.Instance.OnPlayerDie;

        }
        public void SpawnEnemy()
        {
            for (int i = 0; i < numberSpawnEnemys; i++)
            {
                var spawnEnemy = Instantiate(GameManager.Instance.enemy);
                spawnEnemy.Init(GameManager.Instance.enemyHp, GameManager.Instance.enemySpeed, GameManager.Instance.enemyJump);
                spawnEnemy.OnDie += GameManager.Instance.OnEnemyDie;
                spawnEnemy.transform.position = GetEnemyLocation();
            }
        }
        public void SpawnCanonEnemy()
        {
            for (int i = 0; i < numberSpawnCanonEnemys; i++)
            {
                var spawnCanonEnemy = Instantiate(GameManager.Instance.enemyCanon);
                spawnCanonEnemy.Init(GameManager.Instance.canonEmemyHp, GameManager.Instance.canonEmemySpeed, GameManager.Instance.canonEmemyJump);
                spawnCanonEnemy.OnDie += GameManager.Instance.OnEnemyDie;
                spawnCanonEnemy.transform.position = GetCanonLocation();
            }
        }
        public void SpawnMapsLevelOne()
        {
            for (int i = 0; i < numberSpawnMaps; i++)
            {
                var spawnMap = Instantiate(map[0]);
                spawnMap.transform.position = GetSpawnMapLocationLevelOne();
            }
        }
        public void SpawnMapsLevelTwo()
        {
            for (int i = 0; i < numberSpawnMaps; i++)
            {
                var spawnMap = Instantiate(map[1]);
                spawnMap.transform.position = GetSpawnMapLocationLevelTwo();
            }
        }
        public void SpawnHeath()
        {
            for (int i = 0; i < numberSpawnHealth; i++)
            {
                var spawnHealth = Instantiate(heath);
                spawnHealth.transform.position = GetHealthLocation();
            }
        }
        public Vector3 GetSpawnMapLocationLevelOne()
        {
            var mapLocation = spawnMapLevelOnePoints[UnityEngine.Random.Range(0, spawnMapLevelOnePoints.Count)];
            spawnMapLevelOnePoints.Remove(mapLocation);
            return mapLocation.position;
        }
        public Vector3 GetSpawnMapLocationLevelTwo()
        {
            var mapLocation = spawnMapLevelTwoPoints[UnityEngine.Random.Range(0, spawnMapLevelTwoPoints.Count)];
            spawnMapLevelTwoPoints.Remove(mapLocation);
            return mapLocation.position;
        }
        public Vector3 GetEnemyLocation()
        {
            if (spawnEnemyPoints.Count <= 0)
            {
                return Vector3.zero;
            }
            var enemyLocation = spawnEnemyPoints[UnityEngine.Random.Range(0, spawnEnemyPoints.Count)];
            spawnEnemyPoints.Remove(enemyLocation);
            return enemyLocation.position;
        }
        public Vector3 GetHealthLocation()
        {
            var healthLocation = spawnHealthPoionts[UnityEngine.Random.Range(0, spawnHealthPoionts.Count)];
            spawnHealthPoionts.Remove(healthLocation);
            return healthLocation.position;
        }
        public Vector3 GetCanonLocation()
        {
            if (spawnCanonPoints.Count <= 0)
            {
                return Vector3.zero;
            }
            var canonLocation = spawnCanonPoints[UnityEngine.Random.Range(0, spawnCanonPoints.Count)];
            spawnCanonPoints.Remove(canonLocation);
            return canonLocation.position;
        }
    }
}

