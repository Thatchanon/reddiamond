﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Manager;
public class ProjectTile : MonoBehaviour
{
    [SerializeField] private Rigidbody bulletPrefabs;
    [SerializeField] private Transform shootPoint;
    [SerializeField] private float lookRadius;
    [SerializeField] private int timeFire;
    [SerializeField] private float coolDown;
    void Update()
    {
        LaunchProjectile();
    }
    private void LaunchProjectile()
    {
        var target = FindObjectOfType<Player>();
        if (target == null)
        {
            return;
        }
        var distance = Vector3.Distance(transform.position, target.transform.position);
        if (distance < lookRadius)
        {
            coolDown += Time.deltaTime;
            Vector3 vo = CalculateVelocity(target.transform.position, shootPoint.position, 1f);
            Debug.Log(target.transform.position);
            transform.rotation = Quaternion.LookRotation(vo);
            if(coolDown >= timeFire)
            {
                Rigidbody fire = Instantiate(bulletPrefabs, shootPoint.position, Quaternion.identity);
                fire.velocity = vo;
                SoundManager.Instance.Play(SoundManager.Instance.audioSourceAction, SoundManager.Sound.Enemyfire);
                coolDown = 0;
            }
        }
    }
    private Vector3 CalculateVelocity(Vector3 target, Vector3 origin, float time)
    {
        //define the distance x and y first
        Vector3 distance = target - origin;
        Vector3 distance_x_z = distance;
        distance_x_z.Normalize();
        distance_x_z.y = 0;

        //creating a float that represents our distance 
        float sy = distance.y;
        float sxz = distance.magnitude;


        //calculating initial x velocity
        //Vx = x / t
        float Vxz = sxz / time;

        ////calculating initial y velocity
        //Vy0 = y/t + 1/2 * g * t
        float Vy = sy / time + 0.5f * Mathf.Abs(Physics.gravity.y) * time;

        Vector3 result = distance_x_z * Vxz;
        result.y = Vy;
        return result;
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }
}
