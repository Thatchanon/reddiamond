﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointDie : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        var target = other.gameObject.GetComponent<IDamageable>();
        target.TakeHit(100);
    }
}
