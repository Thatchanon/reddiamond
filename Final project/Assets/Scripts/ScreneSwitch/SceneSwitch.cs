﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Manager;
public class SceneSwitch : MonoBehaviour
{
    [SerializeField] private Countinend countinend;
    private void OnTriggerEnter(Collider other)
    {
        countinend.dialogContined.gameObject.SetActive(true);
        Destroy(gameObject);
    }
}
